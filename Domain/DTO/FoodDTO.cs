﻿namespace Domain.DTO
{
    public class FoodDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }
        public int Count { get; set; }
    }
}

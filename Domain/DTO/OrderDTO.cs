﻿using System;

namespace Domain.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int TableNumber { get; set; }
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfEdite { get; set; }
        public int Status { get; set; }
        public string UserId { get; set; }
        public int Price { get; set; }
        public bool IsDelete { get; set; }
    }
}

﻿namespace Domain.DTO
{
    public class OrderFoodDTO
    {
        public int Id { get; set; }
        public int FoodId { get; set; }
        public FoodDTO FoodDto { get; set; }
        public int OrderId { get; set; }
        public OrderDTO OrderDto { get; set; }
        public int Count { get; set; }
    }
}

﻿using System;

namespace Domain.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int TableNumber { get; set; }
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfEdite { get; set; }
        public int Price { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class OrderFood
    {
        [Key]
        public int Id { get; set; }

        public int FoodId { get; set; }
        public Food Food { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int Count { get; set; }
    }
}

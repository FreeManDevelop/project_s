﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enum
{
    public enum RoleEnum
    {
        waiter = 2,
        cook   = 3,
        admin  = 1
    }
}

﻿namespace Domain.Enum
{
    public enum StatusEnum
    {
        Accepted = 1,
        Ready    = 2,
        Filed    = 3,
        Paid     = 4
    }
}

﻿namespace Domain.Infrastructure
{
    public class OperationDetailse
    {
        public bool Succedeed { get; private set; }
        public string Message { get; set; }
        public string Property { get; set; }

        public OperationDetailse(bool succedeed, string message, string property)
        {
            Succedeed = succedeed;
            Message = message;
            Property = property;
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.DTO;
using Domain.Infrastructure;

namespace Domain.InterfaceServices
{
    public interface IFoodService : IDisposable
    {
        Task<OperationDetailse> AddFood(FoodDTO foodDto);
        IQueryable<FoodDTO> GetFoods();
        OperationDetailse RemoveFood(int id);
        OperationDetailse RestoreFood(int id);
        OperationDetailse EditFood(FoodDTO foodDto);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.DTO;
using Domain.Infrastructure;

namespace Domain.InterfaceServices
{
    public interface IMenuService : IDisposable
    {
        OperationDetailse ChangeStatusOrder(int orderId, int role);
        OperationDetailse AddFoodInOrder(List<int> foodsId, List<int> amountFood, int orderId);
        OperationDetailse AddOrder(OrderDTO orderDto, List<int> foodId, List<int> amountFood);
        OperationDetailse EditOrder(OrderDTO orderDto,
            List<int> deleteFoodsId, List<int> deleteFoodsAmounts,
            List<int> addFoodsId, List<int> addFoodsAmount);
        IQueryable<OrderDTO> GetOrders();
        IQueryable<OrderFoodDTO> GetOrderFoodDtos();
        OperationDetailse DeleteOrder(int orderId);
        OperationDetailse RestoreOrder(int orderId);
    }
}

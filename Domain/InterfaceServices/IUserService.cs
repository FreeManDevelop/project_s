﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.DTO;
using Domain.Infrastructure;

namespace Domain.InterfaceServices
{
    public interface IUserService : IDisposable
    {
        Task<OperationDetailse> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task<UserDTO> GetUserAsync(string email);
        //This is method for initializing admin and role collection on start programm
        Task SetInitialData(UserDTO adminDto, List<string> roles);
        Task<OperationDetailse> Remove(string userId);
        Task<OperationDetailse> Edit(UserDTO userDto);
        IQueryable<UserDTO> GetAllUser();
    }
}

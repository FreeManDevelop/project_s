﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.InterfaceServices.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
        void Create(TEntity item);
        void CreateRange(IEnumerable<TEntity> items);
        void Delete(TEntity item);
        void AddOrUpdate(TEntity item);
    }
}

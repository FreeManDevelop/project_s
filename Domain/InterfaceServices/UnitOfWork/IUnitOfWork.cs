﻿using System;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Identity;
using Domain.InterfaceServices.Repository;

namespace Domain.InterfaceServices.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IGenericRepository<Order> Order { get; }
        IGenericRepository<Food> Food { get; }
        IGenericRepository<OrderFood> OrderFood { get; }
        Task SaveAsync();
        void Save();
    }
}

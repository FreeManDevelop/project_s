﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Restaurant.BLL.Extensions
{
    public class MappingExt
    {
        //TR - return type, TM - mapping type
        public static IQueryable<TR> GetMappingCollection<TR, TM>(IQueryable<TM> mappsObj)
            where TR : class
            where TM : class
        {
            var mapper = new MapperConfiguration(cnf => cnf.CreateMap<TM, TR>()).CreateMapper();
            return mapper.Map<IEnumerable<TM>, IEnumerable<TR>>(mappsObj).AsQueryable();
        }

        public static TR GetMappingObj<TR, TM>(TM mapObj)
            where TR : class
            where TM : class 
        {
            var mapper = new MapperConfiguration(cnf => cnf.CreateMap<TM, TR>()).CreateMapper();
            return mapper.Map<TM, TR>(mapObj);
        }
    }
}

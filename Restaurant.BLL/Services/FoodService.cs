﻿using System.Linq;
using System.Threading.Tasks;
using Domain.DTO;
using Domain.Entities;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using Domain.InterfaceServices.UnitOfWork;
using Restaurant.BLL.Extensions;

namespace Restaurant.BLL.Services
{
    public class FoodService : IFoodService
    {
        private readonly IUnitOfWork _uow;

        public FoodService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<OperationDetailse> AddFood(FoodDTO foodDto)
        {
            Food food = _uow.Food.GetAll().FirstOrDefault(f => f.Name == foodDto.Name);

            if (food == null)
            {
                food = new Food
                {
                    Name        = foodDto.Name,
                    Description = foodDto.Description,
                    Price       = foodDto.Price,
                    IsDelete    = false
                };

                _uow.Food.Create(food);
                await _uow.SaveAsync();
                return new OperationDetailse(true, $"You are created {foodDto.Name} successfully!", "");
            }
            else
                return new OperationDetailse(false, $"{foodDto.Name} already exists!", "");
        }

        public IQueryable<FoodDTO> GetFoods()
        {
            return MappingExt.GetMappingCollection<FoodDTO, Food>(_uow.Food.GetAll());
        }

        public OperationDetailse RemoveFood(int id)
        {
            Food food = _uow.Food.GetAll().FirstOrDefault(f => f.Id == id);

            if (food != null)
            {
                food.IsDelete = true;
                _uow.Food.AddOrUpdate(food);
                _uow.Save();

                return new OperationDetailse(true,$"{food.Name} deleted successfully","");
            }
            else
                return new OperationDetailse(false,$"{id} dish not found","");
        }

        public OperationDetailse RestoreFood(int id)
        {
            Food food = _uow.Food.GetAll().FirstOrDefault(f => f.Id == id);

            if (food != null)
            {
                food.IsDelete = false;
                _uow.Food.AddOrUpdate(food);
                _uow.Save();

                return new OperationDetailse(true, $"{food.Name} restored successfully", "");
            }
            else
                return new OperationDetailse(false, $"{id} dish not found", "");
        }

        public OperationDetailse EditFood(FoodDTO foodDto)
        {
            Food food = _uow.Food.GetAll().FirstOrDefault(f => f.Id == foodDto.Id);

            if (food != null)
            {
                Food foodNameUniqe =
                    _uow.Food.GetAll().FirstOrDefault(f => f.Name == foodDto.Name && f.Id != foodDto.Id);

                if (foodNameUniqe == null)
                {
                    food.Name = foodDto.Name;
                    food.Price = foodDto.Price;
                    food.Description = foodDto.Description;
                }
                else
                    return new OperationDetailse(false, $"{foodDto.Name} name already exists!", "");
                _uow.Food.AddOrUpdate(food);
                _uow.Save();
                return new OperationDetailse(true, $"{foodDto.Name} edited successfully!", "");
            }
            else
                return new OperationDetailse(false, $"{foodDto.Name} was not found!", "");
        }

        public void Dispose()
        {
            _uow.Dispose();
        }
    }
}

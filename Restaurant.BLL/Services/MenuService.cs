﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.DTO;
using Domain.Entities;
using Domain.Enum;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using Domain.InterfaceServices.UnitOfWork;
using Restaurant.BLL.Extensions;

namespace Restaurant.BLL.Services
{
    public class MenuService : IMenuService
    {
        private readonly IUnitOfWork _uow;

        public MenuService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void Dispose()
        {
            _uow.Dispose();
        }

        public OperationDetailse ChangeStatusOrder(int orderId, int role)
        {
            Order order = _uow.Order.GetAll().FirstOrDefault(o => o.Id == orderId);
            
            if (order != null)
            {
                switch ((StatusEnum)order.Status)
                {
                    case StatusEnum.Accepted:
                    {
                        if (role == (int)RoleEnum.cook)
                        {
                            order.Status = (int)StatusEnum.Ready;
                            break;
                        }
                        else
                            return new OperationDetailse(false, "You are can't edit this order!", "");
                    }
                    case StatusEnum.Ready:
                    {
                        if (role == (int)RoleEnum.waiter)
                        {
                            order.Status = (int)StatusEnum.Filed;
                            break;
                        }
                        else
                            return new OperationDetailse(false, "You are can't edit this order!", "");
                    }
                    case StatusEnum.Filed:
                    {
                        if (role == (int)RoleEnum.waiter)
                        {
                            order.Status = (int)StatusEnum.Paid;
                            break;
                        }
                        else
                            return new OperationDetailse(false, "You are can't edit this order!", "");
                    }
                }
                _uow.Order.AddOrUpdate(order);
                _uow.Save();

                return new OperationDetailse(true, "Change was order status success!", "");
            }
            else
                return new OperationDetailse(false, "Order was not found!", "");
        }

        public  OperationDetailse AddFoodInOrder(List<int> foodsId, List<int> amountFood, int orderId)
        {
            try
            {
                if (foodsId != null)
                {
                    IEnumerable<OrderFood> orderFoods = _uow.OrderFood.GetAll().Where(o => o.OrderId == orderId);

                    for (int i = 0; i < foodsId.Count; i++)
                    {
                        var result = orderFoods.FirstOrDefault(o => o.FoodId == foodsId[i]);

                        if (result != null)
                        {
                            result.Count += amountFood[i];
                            _uow.OrderFood.AddOrUpdate(result);
                        }
                        else
                            _uow.OrderFood.Create(new OrderFood { FoodId = foodsId[i], OrderId = orderId, Count = amountFood[i] });

                        _uow.Save();
                    }
                    return new OperationDetailse(true, "Dishes added successfully!", "");
                }
                return new OperationDetailse(true, "You did not add any dishes", "Created order");
            }
            catch (Exception)
            {
                return new OperationDetailse(false, "Error adding!", "");
            }
            
        }

        public OperationDetailse AddOrder(OrderDTO orderDto, List<int> FoodsId, List<int> amountFood)
        {
            Order order = new Order
            {
                DateOfCreate = orderDto.DateOfCreate,
                Status       = orderDto.Status,
                TableNumber  = orderDto.TableNumber,
                UserId       = orderDto.UserId,
                DateOfEdite  = orderDto.DateOfEdite
            };
            _uow.Order.Create(order);
            _uow.Save();

            return AddInFoodsForOrder(FoodsId, amountFood, order, $"Order for {order.TableNumber} successfully created!");
        }

        public OperationDetailse EditOrder(OrderDTO orderDto, List<int> deleteFoodsId, List<int> deleteFoodsAmounts, List<int> addFoodsId, List<int> addFoodsAmount)
        {
            Order order = _uow.Order.GetAll().FirstOrDefault(o => o.Id == orderDto.Id);

            if (order != null) 
            {
                order.DateOfEdite = DateTime.Now;

                if (deleteFoodsId != null && deleteFoodsAmounts != null)
                {
                    IQueryable<OrderFood> orderFoods = _uow.OrderFood.GetAll();

                    for (int i = 0; i < deleteFoodsId.Count; i++)
                    {
                        int deleteFoodId = deleteFoodsId[i];

                        OrderFood orderfood = orderFoods.FirstOrDefault(f => f.FoodId == deleteFoodId && f.OrderId == order.Id);

                        if (orderfood != null)
                        {
                            if (orderfood.Count == deleteFoodsAmounts[i])
                            {
                                _uow.OrderFood.Delete(orderfood);
                                deleteFoodsAmounts[i] = -1;
                            }
                            if (deleteFoodsAmounts[i] > 0)
                            {
                                orderfood.Count -= deleteFoodsAmounts[i];
                                _uow.OrderFood.AddOrUpdate(orderfood);
                            }
                            if (deleteFoodsAmounts[i] == 0)
                            {
                                continue;
                            }
                        }
                        _uow.Save();
                    }

                    order.Price = SumFoodOrder(order.Id);
                    _uow.Order.AddOrUpdate(order);
                }
                if (addFoodsId != null && addFoodsAmount != null)
                {
                    return AddInFoodsForOrder(addFoodsId, addFoodsAmount, order, "Successfully added or delete!");
                }

                _uow.Save();

                return new OperationDetailse(true, $"{orderDto.TableNumber} successfully delete or delete!","");
            }
            else
                return new OperationDetailse(false, $"{orderDto.Id} not found!","");
        }

        public IQueryable<OrderDTO> GetOrders()
        {
            return MappingExt.GetMappingCollection<OrderDTO, Order>(_uow.Order.GetAll());
        }

        public IQueryable<OrderFoodDTO> GetOrderFoodDtos()
        {
            return MappingExt.GetMappingCollection<OrderFoodDTO, OrderFood>(_uow.OrderFood.GetAll());
        }

        public OperationDetailse DeleteOrder(int orderId)
        {
            Order order = _uow.Order.GetAll().FirstOrDefault(o => o.Id == orderId);

            if (order != null)
            {
                order.IsDelete = true;

                _uow.Order.AddOrUpdate(order);
                _uow.Save();

                return new OperationDetailse(true, $"{orderId} is deleted!", "");
            }
            else
                return new OperationDetailse(false, $"{orderId} was not found!", "");
        }

        public OperationDetailse RestoreOrder(int orderId)
        {
            Order order = _uow.Order.GetAll().FirstOrDefault(o => o.Id == orderId);

            if (order != null)
            {
                order.IsDelete = false;

                _uow.Order.AddOrUpdate(order);
                _uow.Save();

                return new OperationDetailse(true, $"{orderId} is restored!", "");
            }
            else
                return new OperationDetailse(false, $"{orderId} was not found!", "");
        }

        #region Helpers

        private OperationDetailse AddInFoodsForOrder(List<int> foodsId, List<int> amountFood, Order order, string successMessage)
        {
            OperationDetailse operationDetailse = AddFoodInOrder(foodsId, amountFood, order.Id);

            if (operationDetailse.Succedeed && operationDetailse.Property == string.Empty)
            {
                order.Price = SumFoodOrder(order.Id);
                _uow.Order.AddOrUpdate(order);
                _uow.Save();

                return new OperationDetailse(true, successMessage, "");
            }
            else
                return operationDetailse;
        }

        private int SumFoodOrder(int orderId)
        {
            int sumCounter = 0;

            var sumFoods =
                from f in _uow.OrderFood.GetAll().Where(o => o.OrderId == orderId).Include(f => f.Food)
                select new
                {
                    isDelete = f.Food.IsDelete,
                    price    = f.Food.Price,
                    count    = f.Count,
                    id       = f.Id
                };

            var groupFoods = sumFoods.GroupBy(sF => sF.id);
            
            foreach (var food in groupFoods)
            {
                sumCounter += food.Sum(f => f.price) * food.Sum(f => f.count);
            }

            return sumCounter;
        }

        #endregion
    }
}

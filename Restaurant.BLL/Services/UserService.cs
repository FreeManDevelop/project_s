﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Domain.DTO;
using Domain.Entities;
using Domain.Enum;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using Domain.InterfaceServices.UnitOfWork;
using Microsoft.AspNet.Identity;
using Restaurant.BLL.Extensions;

namespace Restaurant.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;

        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void Dispose()
        {
            _uow.Dispose();
        }

        public async Task<OperationDetailse> Create(UserDTO userDto)
        {
            ApplicationUser user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Email == userDto.Email);

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = userDto.UserName,
                    Email    = userDto.Email,
                    Name     = userDto.Name,
                    Address  = userDto.Address,
                    Role     = userDto.Role
                };
                var result = await _uow.UserManager.CreateAsync(user, userDto.Password);

                if (result.Errors.Any())
                {
                    return new OperationDetailse(false, result.Errors.FirstOrDefault(), "");
                }

                await _uow.UserManager.AddToRoleAsync(user.Id, ((RoleEnum)userDto.Role).ToString());
                await _uow.SaveAsync();
                return new OperationDetailse(true, "Creation of the worker was successful!", "");
            }
            else
            {
                return new OperationDetailse(false,"Worker with any email already exists!","");
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claims = null;

            ApplicationUser user = await _uow.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
            {
                claims = await _uow.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            }

            return claims;
        }

        public async Task<UserDTO> GetUserAsync(string email)
        {
            ApplicationUser user = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Email == email || u.UserName == email);

            return MappingExt.GetMappingObj<UserDTO, ApplicationUser>(user);
        }

        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await _uow.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await _uow.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }

        public async Task<OperationDetailse> Remove(string userId)
        {
            ApplicationUser user = await _uow.UserManager.FindByIdAsync(userId);

            if (user != null)
            {
                var result = await _uow.UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return new OperationDetailse(true, $"{user.Name} deleted successfully!","");
                }
                else
                    return new OperationDetailse(false, $"Error: {result.Errors.First()}", "");
            }
            else
                return new OperationDetailse(false, $"{userId} this is user not found!","");
        }

        public async Task<OperationDetailse> Edit(UserDTO userDto)
        {
            ApplicationUser user = await _uow.UserManager.FindByIdAsync(userDto.Id);

            if (user != null)
            {
                ApplicationUser userEmailUniqe = await _uow.UserManager.Users.FirstOrDefaultAsync(u => u.Email == userDto.Email && u.Id != userDto.Id);

                if(userEmailUniqe != null)
                    return new OperationDetailse(false, $"Email address already exists!", "");

                user.Name         = userDto.Name;
                user.Email        = userDto.Email;
                user.UserName     = userDto.Email;
                user.Address      = userDto.Address;
                user.PasswordHash = _uow.UserManager.PasswordHasher.HashPassword(userDto.Password);

                var deleteRoleRes = await _uow.UserManager.RemoveFromRoleAsync(user.Id, ((RoleEnum)user.Role).ToString());

                if (deleteRoleRes.Succeeded)
                {
                    var addRoleRes = await _uow.UserManager.AddToRoleAsync(user.Id, ((RoleEnum)userDto.Role).ToString());

                    if (addRoleRes.Succeeded)
                    {
                        user.Role = userDto.Role;

                        var result = await _uow.UserManager.UpdateAsync(user);

                        if (result.Succeeded)
                            return new OperationDetailse(true, $"User {userDto.Name} successfully edited!", "");
                        else
                            return new OperationDetailse(false, $"Error: {result.Errors.First()}", "");
                    }
                    else
                        return new OperationDetailse(false, $"Error: {addRoleRes.Errors.First()}", "");

                }
                else
                    return new OperationDetailse(false, $"Error: {deleteRoleRes.Errors.First()}", "");
            }
            else
                return new OperationDetailse(false, $"User {userDto.Id} was not found!", "");
        }

        public IQueryable<UserDTO> GetAllUser()
        {
            IQueryable<ApplicationUser> users = _uow.UserManager.Users.Where(u => u.UserName != "admin@mail.ru" && u.Email != "admin@mail.ru");

            return MappingExt.GetMappingCollection<UserDTO, ApplicationUser>(users);
        }
    }
}

﻿using System.Data.Entity;
using Domain.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Restaurant.DAL.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("name=ConnectDb")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ApplicationDbContext>());
        }

        public DbSet<Food> Foods { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderFood> OrderFoods { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Food>()
                .ToTable("Dishe")
                .HasKey(f => f.Id);

            modelBuilder.Entity<Food>()
                .Property(p => p.Name)
                .HasColumnName("Name dish")
                .HasMaxLength(50);

            modelBuilder.Entity<Food>()
                .Property(p => p.IsDelete)
                .HasColumnName("Is delete");

            modelBuilder.Entity<Order>()
                .HasKey(p => p.Id)
                .HasRequired(p => p.User).WithMany()
                .HasForeignKey(p => p.UserId);

            modelBuilder.Entity<Order>()
                .Property(p => p.DateOfCreate)
                .HasColumnName("Date of create");

            modelBuilder.Entity<Order>()
                .Property(p => p.DateOfEdite)
                .HasColumnName("Date of edite");

            modelBuilder.Entity<OrderFood>()
                .ToTable("Order Food")
                .HasRequired(p => p.Food).WithMany().HasForeignKey(p => p.FoodId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderFood>()
                .HasRequired(p => p.Order).WithMany().HasForeignKey(p => p.OrderId)
                .WillCascadeOnDelete(false);
        }
    }
}

namespace Restaurant.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_IsDelete_Field_Order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsDelete");
        }
    }
}

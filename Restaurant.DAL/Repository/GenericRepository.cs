﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Domain.InterfaceServices.Repository;
using Restaurant.DAL.Data;

namespace Restaurant.DAL.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ApplicationDbContext context)
        {
            _dbSet = context.Set<TEntity>();
        }

        public TEntity Get(int id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsNoTracking();
        }

        public void Create(TEntity item)
        {
            _dbSet.Add(item);
        }

        public void CreateRange(IEnumerable<TEntity> items)
        {
            _dbSet.AddRange(items);
        }

        public void Delete(TEntity item)
        {
            _dbSet.Attach(item);
            _dbSet.Remove(item);
        }

        public void AddOrUpdate(TEntity item)
        {
            _dbSet.AddOrUpdate(item);
        }
    }
}

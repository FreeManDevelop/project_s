﻿using System;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Identity;
using Domain.InterfaceServices.Repository;
using Domain.InterfaceServices.UnitOfWork;
using Microsoft.AspNet.Identity.EntityFramework;
using Restaurant.DAL.Data;
using Restaurant.DAL.Repository;

namespace Restaurant.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationRoleManager _roleManager;
        private readonly ApplicationUserManager _userManager;
        private GenericRepository<Food> _food;
        private GenericRepository<Order> _order;
        private GenericRepository<OrderFood> _orderFood;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
            _roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_context));
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_context));
        }

        public void Dispose()
        {
            _roleManager.Dispose();
            _userManager.Dispose();
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager; }
        }

        public IGenericRepository<Order> Order
        {
            get
            {
                if(_order == null)
                    _order = new GenericRepository<Order>(_context);
                return _order;
            }
        }

        public IGenericRepository<Food> Food
        {
            get
            {
                if(_food == null)
                    _food = new GenericRepository<Food>(_context);
                return _food;
            }
        }

        public IGenericRepository<OrderFood> OrderFood
        {
            get
            {
                if(_orderFood == null)
                    _orderFood = new GenericRepository<OrderFood>(_context);
                return _orderFood;
            }
        }

        public Task SaveAsync()
        {
           return _context.SaveChangesAsync();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}

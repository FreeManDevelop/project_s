using System.Web.Mvc;
using Domain.InterfaceServices;
using Domain.InterfaceServices.UnitOfWork;
using Restaurant.BLL.Services;
using Restaurant.DAL.UnitOfWork;
using Unity;
using Unity.Mvc5;

namespace Restaurant.UI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IMenuService, MenuService>();
            container.RegisterType<IFoodService, FoodService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
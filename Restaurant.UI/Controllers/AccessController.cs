﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.DTO;
using Domain.Enum;
using Domain.InterfaceServices;
using Microsoft.Owin.Security;
using Restaurant.BLL.Services;
using Restaurant.UI.ViewModels;

namespace Restaurant.UI.Controllers
{
    public class AccessController : Controller
    {
        private readonly IUserService _userService;

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public AccessController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await _userService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", @"Wrong login or password.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = false
                    }, claim);

                    return RedirectToAction("ShowAllOrders", "Order");
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("ShowAllOrders", "Order");
        }

        private async Task SetInitialDataAsync()
        {
            await _userService.SetInitialData(new UserDTO
            {
                Email    = "admin@mail.ru",
                UserName = "admin@mail.ru",
                Password = "12qw!@QW",
                Name     = "Admin",
                Role     = (int)RoleEnum.admin
            }, new List<string> { "waiter", "admin", "cook" });
        }
    }
}
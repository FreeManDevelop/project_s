﻿using System.Web.Mvc;

namespace Restaurant.UI.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return RedirectToAction("Login", "Access");
        }
    }
}
﻿using System.Web.Mvc;

namespace Restaurant.UI.Controllers
{
    public class CustomErrorController : Controller
    {
        public ActionResult ErrorServer()
        {
            ViewBag.StatusCode = "500";
            ViewBag.MessageError = "Internal Server Error";

            return View("Error");
        }

        public ActionResult PageNotFound()
        {
            ViewBag.StatusCode = "404";
            ViewBag.MessageError = "Not Found";

            return View("Error");
        }

        public ActionResult BadRequest()
        {
            ViewBag.StatusCode = "400";
            ViewBag.MessageError = "Bad Request";

            return View("Error");
        }
    }
}
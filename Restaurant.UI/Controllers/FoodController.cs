﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.DTO;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using Microsoft.Ajax.Utilities;
using PagedList;
using Restaurant.UI.Extension;
using Restaurant.UI.ViewModels;

namespace Restaurant.UI.Controllers
{
    [Authorize]
    public class FoodController : Controller
    {
        private readonly IFoodService _foodService;

        public FoodController(IFoodService foodService)
        {
            _foodService   = foodService;
        }

        [Authorize(Roles = "admin")]
        public ActionResult CreateFood()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> CreateFood(CreateFoodViewModel model)
        {
            if (ModelState.IsValid)
            {
                FoodDTO foodDto = new FoodDTO
                {
                    Name        = model.Name,
                    Description = model.Description,
                    Price       = model.Price
                };

                return OperationDetailseRes(await _foodService.AddFood(foodDto), model, "Food", "ShowAllFoods");
            }

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteFood(int id)
        {
            return OperationDetailseRes(_foodService.RemoveFood(id), id, "Food", "ShowAllFoods");
        }

        [Authorize(Roles = "admin")]
        public ActionResult RestoreFood(int id)
        {
            return OperationDetailseRes(_foodService.RestoreFood(id), id, "Food", "ShowAllFoods");
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditFood(int id)
        {
            FoodDTO food = _foodService.GetFoods().FirstOrDefault(f => f.Id == id);

            if (food != null)
            {
                return View(food);
            }
            else
            {
                ModelState.AddModelError(string.Empty, $"{id} was not found!");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult EditFood(FoodDTO foodDto)
        {
            return OperationDetailseRes(_foodService.EditFood(foodDto), foodDto, "Food", "ShowAllFoods");
        }

        public ActionResult ShowAllFoods(FilterViewModel filter, string message = "", int page = 1)
        {
            IQueryable<FoodDTO> foods = null;
            ViewBag.Message = message;

            if (!User.IsInRole("admin"))
                foods = _foodService.GetFoods().Where(f => !f.IsDelete);
            else
                foods = _foodService.GetFoods();

            if (foods.Any())
            {
                foods = ForFilterExt.FilterForFood(filter, foods);

                PagingViewModel<FoodDTO> viewModel = new PagingViewModel<FoodDTO>
                {
                    FilterViewModel = filter,
                    Objects         = foods.ToPagedList(page, filter.PageSize <= 0 ? 3 : filter.PageSize)
                };

                return View(viewModel);
            }

            return View(new PagingViewModel<FoodDTO> { FilterViewModel = filter });
        }

        #region Helpers

        private ActionResult OperationDetailseRes<T>(OperationDetailse operationDetailse, T model, string controller, string action)
        {
            if (operationDetailse.Succedeed)
            {
                return RedirectToAction(action, controller, new { message = operationDetailse.Message });
            }
            else
            {
                ModelState.AddModelError(operationDetailse.Property, operationDetailse.Message);
                return View(model);
            }
        }

        #endregion
    }
}
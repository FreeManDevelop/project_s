﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.DTO;
using Domain.Enum;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using PagedList;
using Restaurant.UI.Extension;
using Restaurant.UI.ViewModels;
using Restaurant.UI.ViewModels.OrderViewModels;

namespace Restaurant.UI.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IMenuService _menuService;
        private readonly IUserService _userService;
        private readonly IFoodService _foodService;

        public OrderController(IMenuService menuService, IUserService userService, IFoodService foodService)
        {
            _menuService = menuService;
            _userService = userService;
            _foodService = foodService;
        }

        [Authorize(Roles = "waiter")]
        public ActionResult CreateOrder()
        {
            ViewBag.Foods = new SelectList(_foodService.GetFoods().Where(f => !f.IsDelete), "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "waiter")]
        public async Task<ActionResult> CreateOrder(CreateOrderViewModel model)
        {
            ViewBag.Foods = new SelectList(_foodService.GetFoods().Where(f => !f.IsDelete), "Id", "Name");

            if (ModelState.IsValid)
            {
                UserDTO user = await _userService.GetUserAsync(User.Identity.Name);

                OrderDTO order = new OrderDTO
                {
                    DateOfCreate = DateTime.Now,
                    TableNumber  = model.NumberTable,
                    Status       = (int)StatusEnum.Accepted,
                    UserId       = user.Id,
                    DateOfEdite  = DateTime.Now
                };

                return OperationDetailseRes(_menuService.AddOrder(order, model.FoodId, model.AmountFood), model, "Order", "ShowAllOrders");
            }

            return View(model);
        }

        [Authorize(Roles = "waiter")]
        public ActionResult EditOrder(int id)
        {
            OrderDTO orderDto = _menuService.GetOrders().FirstOrDefault(o => o.Id == id);

            if (orderDto != null)
            {
                ViewBag.DeleteFoods = GetFoodsForOrderFilter(o => o.OrderId == orderDto.Id);
                ViewBag.AddFoods = new SelectList(_foodService.GetFoods().Where(f => !f.IsDelete), "Id", "Name");

                return View(orderDto);
            }

            ModelState.AddModelError(string.Empty, @"Order not found!");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "waiter")]
        public ActionResult EditOrder(EditeViewModel model)
        {
            ViewBag.DeleteFoods = GetFoodsForOrderFilter(o => o.OrderId == model.Id);
            ViewBag.AddFoods = new SelectList(_foodService.GetFoods().Where(f => !f.IsDelete), "Id", "Name");

            return OperationDetailseRes(_menuService.EditOrder(new OrderDTO{Id = model.Id, TableNumber = model.TableNumber}, model.DeleteFoodId, model.DeleteFoodAmount, model.FoodId, model.AmountFood), model, "Order", "ShowAllOrders");
        }

        [Authorize(Roles = "waiter")]
        public ActionResult DeleteOrder(int id)
        {
            return OperationResWithoutView(_menuService.DeleteOrder(id), "Order", "ShowAllOrders");
        }

        [Authorize(Roles = "waiter")]
        public ActionResult RestoreOrder(int id)
        {
            return OperationResWithoutView(_menuService.RestoreOrder(id), "Order", "ShowAllOrders");
        }

        public ActionResult ShowAllOrders( FilterViewModel filter, string message = "", int page = 1)
        {
            ViewBag.Message = message;

            IQueryable<ShowOrderViewModel> showOrderViewModels =
                from order in _menuService.GetOrders()
                join food in _menuService.GetOrderFoodDtos()
                    on order.Id equals food.OrderId into f
                from subFood in f.DefaultIfEmpty()
                select new ShowOrderViewModel
                {
                    TableNumber  = order.TableNumber,
                    Status       = order.Status,
                    Price        = order.Price,
                    DishesCount  = subFood == null ? 0 : subFood.Count,
                    DateOfCreate = order.DateOfCreate,
                    DateOfEdite  = order.DateOfEdite,
                    Id           = order.Id,
                    IsDelete     = order.IsDelete
                };

            if (User.IsInRole("cook"))
                showOrderViewModels = showOrderViewModels.Where(q => q.Status == (int)StatusEnum.Accepted && !q.IsDelete);
            if (User.IsInRole("admin"))
                showOrderViewModels = showOrderViewModels.Where(q => !q.IsDelete);

            if (showOrderViewModels.Any())
            {
                showOrderViewModels = ForFilterExt.FilterForOrder(filter, showOrderViewModels);

                OrderPagingViewModel viewModel = new OrderPagingViewModel
                {
                    FilterViewModel = filter,
                    Objects = showOrderViewModels.OrderByDescending(o => o.DateOfCreate).GroupBy(g => g.Id).ToPagedList(page, filter.PageSize <= 0 ? 3 : filter.PageSize)
                };

                return View(viewModel);
            }

            return View(new OrderPagingViewModel { FilterViewModel = filter });
        }

        public ActionResult ShowFoodsInOrder(int orderId)
        {
            OrderDTO order = _menuService.GetOrders().FirstOrDefault(o => o.Id == orderId);

            if (order != null)
            {
                var foods =
                    from orderFood in _menuService.GetOrderFoodDtos().Where(o => o.OrderId == orderId)
                    join food in _foodService.GetFoods() on orderFood.FoodId equals food.Id
                    select new ShowFoodsViewModel
                    {
                        Description = food.Description,
                        Name        = food.Name,
                        Count       = orderFood.Count,
                        Price       = food.Price
                    };

                return View(foods.AsEnumerable());
            }

            return RedirectToAction("ShowAllOrders", new { message = "Order not found!" });
        }

        #region Helpers

        private ActionResult OperationDetailseRes<T>(OperationDetailse operationDetailse, T model, string control, string action)
        {
            if (operationDetailse.Succedeed)
            {
                return RedirectToAction(action, control, new { message = operationDetailse.Message });
            }
            else
            {
                ModelState.AddModelError(operationDetailse.Property, operationDetailse.Message);
                return View(model);
            }
        }

        private ActionResult OperationResWithoutView(OperationDetailse operationDetailse, string control, string action)
        {
            return RedirectToAction(action, control, new { message = operationDetailse.Message });
        }

        private IEnumerable<FoodDTO> GetFoodsForOrderFilter(Func<OrderFoodDTO, bool> predicate)
        {
            return from orderFood in _menuService.GetOrderFoodDtos().Where(predicate)
                   join food in _foodService.GetFoods() on orderFood.FoodId equals food.Id
                   select new FoodDTO
                   {
                       Description = food.Description,
                       Id = food.Id,
                       IsDelete = food.IsDelete,
                       Name = food.Name,
                       Price = food.Price,
                       Count = orderFood.Count
                   };
        }

        #endregion

    }
}
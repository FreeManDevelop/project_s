﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.DTO;
using Domain.Enum;
using Domain.Infrastructure;
using Domain.InterfaceServices;
using PagedList;
using Restaurant.UI.Extension;
using Restaurant.UI.ViewModels;
using Restaurant.UI.ViewModels.UserViewModels;

namespace Restaurant.UI.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IMenuService _menuService;
        private readonly IUserService _userService;

        public UserController(IUserService userService, IMenuService menuService)
        {
            _userService   = userService;
            _menuService   = menuService;
        }

        [Authorize(Roles = "admin")]
        public ActionResult RegisterUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> RegisterUser(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email    = model.Email,
                    Password = model.Password,
                    Address  = model.Address,
                    Name     = model.Name,
                    UserName = model.Email,
                    Role     = (int)model.Role
                };

                return OperationDetailseRes(await _userService.Create(userDto), model, "User", "ShowAllUser");
            }

            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult ShowAllUser(FilterViewModel model, string message = "", int page = 1)
        {
            IQueryable<UserDTO> users = _userService.GetAllUser();
            ViewBag.Message = message;

            if (users.Any())
            {
                users = ForFilterExt.FilterForUser(model, users);

                PagingViewModel<UserDTO> viewModel = new PagingViewModel<UserDTO>
                {
                    FilterViewModel = model,
                    Objects         = users.ToPagedList(page, 3)
                };

                return View(viewModel);
            }

            return View(new PagingViewModel<UserDTO> { FilterViewModel = model });
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteUser(string userId)
        {
            return OperationDetailseRes(await _userService.Remove(userId), userId, "Admin", "ShowAllUser");
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditUser(string userId)
        {
            UserDTO user = _userService.GetAllUser().FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                return View(new EditUserViewModel
                {
                    UserId  = user.Id,
                    Address = user.Address,
                    Name    = user.Name,
                    Role    = (RoleEnum)user.Role
                });
            }
            else
                return RedirectToAction("ShowAllUser", new { message = $"User {userId} was not found!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                return OperationDetailseRes(await _userService.Edit(new UserDTO
                {
                    Id       = model.UserId,
                    Address  = model.Address,
                    Name     = model.Name,
                    Password = model.Password,
                    Role     = (int)model.Role,
                    Email    = model.Email

                }), model, "User", "ShowAllUser");
            }
            else
                return View(model);
        }

        public ActionResult ChangeStatusOrder(int orderId)
        {
            UserDTO user = _userService.GetAllUser()
                .FirstOrDefault(u => u.Email == User.Identity.Name || u.UserName == User.Identity.Name);

            OperationDetailse operation = _menuService.ChangeStatusOrder(orderId, user.Role);

            return RedirectToAction("ShowAllOrders", "Order", new { message = operation.Message });
        }

        [AllowAnonymous]
        public ActionResult About() => View();

        #region Helpers

        private ActionResult OperationDetailseRes<T>(OperationDetailse operationDetailse, T model, string controller, string action)
        {
            if (operationDetailse.Succedeed)
            {
                return RedirectToAction(action, controller, new { message = operationDetailse.Message });
            }
            else
            {
                ModelState.AddModelError(operationDetailse.Property, operationDetailse.Message);
                return View(model);
            }
        }

        #endregion
    }
}
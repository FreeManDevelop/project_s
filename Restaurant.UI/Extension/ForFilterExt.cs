﻿using System.Linq;
using Domain.DTO;
using Domain.Enum;
using Restaurant.UI.ViewModels;

namespace Restaurant.UI.Extension
{
    public class ForFilterExt
    {
        public static IQueryable<ShowOrderViewModel> FilterForOrder(FilterViewModel filter, IQueryable<ShowOrderViewModel> orders)
        {
            if (filter.SelectedDateCreateTo != null)
            {
                orders = orders.Where(t => t.DateOfCreate <= filter.SelectedDateCreateTo);
            }
            if (filter.SelectedDateCreateFrom != null)
            {
                orders = orders.Where(t => t.DateOfCreate >= filter.SelectedDateCreateFrom);
            }
            if (filter.Status != null)
            {
                orders = orders.Where(t => t.Status == (int)filter.Status);
            }
            if (filter.TableNumber != null)
            {
                orders = orders.Where(t => t.TableNumber == filter.TableNumber);
            }

            return orders;
        }

        public static IQueryable<UserDTO> FilterForUser(FilterViewModel filter, IQueryable<UserDTO> users)
        {
            if (!string.IsNullOrEmpty(filter.Name))
            {
                users = users.Where(u => u.Name.Contains(filter.Name));
            }
            if (!string.IsNullOrEmpty(filter.Email))
            {
                users = users.Where(u => u.Email.Contains(filter.Email));
            }
            if (filter.Role != null)
            {
                users = users.Where(u => u.Role == (int)filter.Role);
            }

            return users;
        }

        public static IQueryable<FoodDTO> FilterForFood(FilterViewModel filter, IQueryable<FoodDTO> foods)
        {
            if (!string.IsNullOrEmpty(filter.Name))
            {
                foods = foods.Where(f => f.Name.Contains(filter.Name));
            }
            if (filter.PriceFrom != null)
            {
                foods = foods.Where(f => f.Price >= filter.PriceFrom);
            }
            if (filter.PriceTo != null)
            {
                foods = foods.Where(f => f.Price <= filter.PriceTo);
            }

            return foods;
        }
    }
}
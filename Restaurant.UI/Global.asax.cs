﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Restaurant.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            UnityConfig.RegisterComponents();
        }

        protected void Application_Error()
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();

            if (exception is HttpException)
            {
                var httpException = (HttpException)exception;
                int code = httpException.GetHttpCode();
                if (code == 400)
                {
                    Response.Redirect("/CustomError/BadRequest");
                }
                if (code == 404)
                {
                    Response.Redirect("/CustomError/PageNotFound");
                }
            }
            else
                Response.Redirect("/CustomError/ErrorServer");
        }

    }
}

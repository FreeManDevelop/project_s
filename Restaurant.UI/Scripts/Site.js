﻿function ChangePageSize(idselect, idInputpage) {
    var size = $(idselect).val();
    $(idInputpage).val(size);
}

//function for creating dynamic fields

function addDynamicExtraField(selectListJson) {

    var select = document.createElement('select');
    select.name = "FoodId";
    select.className = "form-control";
    select.style = "width: 100%";

    for (var i = 0; i < selectListJson.length; i++) {

        var option = document.createElement("option");

        option.setAttribute("value", selectListJson[i].Value);
        option.text = selectListJson[i].Text;

        select.appendChild(option);
    }

    var td = document.createElement('td');
    td.className = "DeleteDynamicExtraField";
    td.colSpan = "2";
    td.appendChild(select);

    var tr = $('<tr/>', {
        'class': 'DynamicExtraField'
    }).appendTo($('#DynamicExtraFieldsContainer'));

    var ListBox = $(td).appendTo(tr);

    var inputForAmountFood = $(
        '<td class="DeleteDynamicExtraField"><input' +
        ' class="form-control "' +
        ' style="width: 70px;"' +
        ' name="AmountFood"' +
        'min="1"' +
        'value="1"' +
        ' type="number" required /></td>').appendTo(tr);

    var input = $('<td class="DeleteDynamicExtraField"> <input' +
        ' style="width: 80px; margin-right: 5px;"' +
        'type="button"' +
        'value="Remove"'+
        'class="btn btn-danger" /></td>').appendTo(tr);

    input.click(function () {
        $(this).parent().remove();
    });
}

$('.DeleteDynamicExtraField').click(function (event) {
    $(this).parent().remove();
    return false;
});
﻿using System.ComponentModel.DataAnnotations;

namespace Restaurant.UI.ViewModels
{
    public class CreateFoodViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}
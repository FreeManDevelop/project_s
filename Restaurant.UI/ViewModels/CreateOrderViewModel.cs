﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Restaurant.UI.ViewModels
{
    public class CreateOrderViewModel
    {
        [Required]
        [Display(Name = "Table number ")]
        public int NumberTable { get; set; }

        public List<int> FoodId { get; set; }

        public List<int> AmountFood { get; set; }
    }
}
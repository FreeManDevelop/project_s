﻿using System;
using Domain.Enum;

namespace Restaurant.UI.ViewModels
{
    public class FilterViewModel
    {
        public DateTime? SelectedDateCreateFrom { get; set; }
        public DateTime? SelectedDateCreateTo { get; set; }
        public int? TableNumber { get; set; }
        public StatusEnum? Status { get; set; }
        public RoleEnum? Role { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }
        public int PageSize { get; set; }
    }
}

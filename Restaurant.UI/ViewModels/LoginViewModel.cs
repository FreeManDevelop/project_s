﻿using System.ComponentModel.DataAnnotations;

namespace Restaurant.UI.ViewModels
{
    public class LoginViewModel
    {
        [EmailAddress]
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
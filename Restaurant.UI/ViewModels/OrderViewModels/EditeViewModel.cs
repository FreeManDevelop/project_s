﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.DTO;

namespace Restaurant.UI.ViewModels.OrderViewModels
{
    public class EditeViewModel
    {
        //OrderDTO orderDto, List<int> deleteFoodsId, List<int> deleteFoodsAmounts, List<int> addFoodsId, List<int> addFoodsAmount

        public int Id { get; set; }
        public int TableNumber { get; set; }

        public List<int> DeleteFoodId { get; set; }
        public List<int> DeleteFoodAmount { get; set; }

        public List<int> FoodId { get; set; }
        public List<int> AmountFood { get; set; }
    }
}
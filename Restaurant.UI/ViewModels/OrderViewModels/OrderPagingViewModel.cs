﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace Restaurant.UI.ViewModels.OrderViewModels
{
    public class OrderPagingViewModel
    {
        public IPagedList<IGrouping<int, ShowOrderViewModel>> Objects { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
    }
}
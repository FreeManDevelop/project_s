﻿using System.Collections.Generic;
using PagedList;

namespace Restaurant.UI.ViewModels
{
    public class PagingViewModel<T>
    {
        public IPagedList<T> Objects { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
    }
}
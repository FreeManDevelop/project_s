﻿using System.ComponentModel.DataAnnotations;
using Domain.Enum;

namespace Restaurant.UI.ViewModels
{
    public class RegisterViewModel
    {
        [EmailAddress]
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Password confirm")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Required]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Role")]
        public RoleEnum Role { get; set; }
    }
}
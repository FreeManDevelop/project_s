﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurant.UI.ViewModels
{
    public class ShowFoodsViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Count { get; set; }
    }
}
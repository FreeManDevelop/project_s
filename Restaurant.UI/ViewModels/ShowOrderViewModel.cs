﻿using System;

namespace Restaurant.UI.ViewModels
{
    public class ShowOrderViewModel
    {
        public int Id { get; set; }
        public int TableNumber { get; set; }
        public int Status { get; set; }
        public int Price { get; set; }
        public int DishesCount { get; set; }
        public DateTime DateOfCreate { get; set; }
        public DateTime DateOfEdite { get; set; }
        public bool IsDelete { get; set; }
    }
}
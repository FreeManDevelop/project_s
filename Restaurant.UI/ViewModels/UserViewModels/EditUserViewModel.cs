﻿using System.ComponentModel.DataAnnotations;
using Domain.Enum;

namespace Restaurant.UI.ViewModels.UserViewModels
{
    public class EditUserViewModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public RoleEnum Role { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }
}